FROM chef/inspec
# Fix "bug" in chef/inspec of not having user 1000.
# Every image needs this user because all volumes mounted into
# a container by docker-compose are owned by user 1000.
RUN adduser -s /sbin/nologin -D -h /share -u 1000 inspec
USER inspec